## Start Project

In the project directory, you can run:

### `npm run dev-server`

Runs the json-web server

### `npm start`

Launches the app. \
The page will reload if you make edits.\
You will also see any lint errors in the console.

