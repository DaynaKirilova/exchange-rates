import { useEffect, useState } from "react";
import "./App.css";
import Rates from "./components/Rates/Rates";
import { GET_CURRENCIES_URL } from "./constants/api";
import { MIN_RATE_VALUE } from "./constants/rates";

function App() {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [rates, setRates] = useState([]);
  const [baseCurrency, setBaseCurrency] = useState(null);

  const loadCurrencies = async () => {
    setLoading(true);
    try {
      const response = await fetch(GET_CURRENCIES_URL, {
        headers: {
          "Content-Type": "application/json",
        },
      }).then((res) => res.json());

      const [{ rates, base }] = response;
      const ratesFormatted = Object.entries(rates).map(
        ([currencyName, rate]) => ({
          currencyName,
          value: Math.max(rate, MIN_RATE_VALUE),
        })
      );

      setRates(ratesFormatted);
      setBaseCurrency(base);
      setError(null);
    } catch (err) {
      setError(err);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    loadCurrencies();
  }, []);

  if (loading) {
    return <h1 className="rate-loading">Loading...</h1>;
  }

  if (error) {
    return (
      <h1 div className="rate-error">
        We were unable to load rates. Please
        <button type="button" onClick={loadCurrencies}>
          try again
        </button>
        later
      </h1>
    );
  }

  return(
     <div className="rate-container">
     <h1 className="rate-heading">Real-time exchange rates.</h1>
  <Rates rates={rates} baseCurrency={baseCurrency} />
  </div>
  )
}

export default App;
