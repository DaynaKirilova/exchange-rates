import "./Rate.css";

export default function Rate({ baseCurrency, currency, value, direction }) {
  return (
    <div className="rate">
      <span>
        {baseCurrency}
        {currency}
      </span>
      <span className={`rate-value rate-${direction}`}>{value.toFixed(4)}</span>
    </div>
  );
}
