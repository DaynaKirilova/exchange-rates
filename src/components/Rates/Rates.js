import { useState, useEffect, useRef } from "react";
import {
  RATE_VALUE_CHANGE_INTERVAL,
  RATE_VALUE_CHANGE_STEP,
  RATE_VALUE_DIRECTION,
  RATE_VALUE_DIRECTION_INTERVAL,
  RATE_VALUE_STOP_CHANGE_TIMEOUT,
} from "../../constants/rates";
import Rate from "../Rate/Rate";

export default function Rates({ rates, baseCurrency }) {
  const updateRatesInterval = useRef();
  const changeDirectionInterval = useRef();
  const stopRateUpdatesTimeout = useRef();
  const direction = useRef(RATE_VALUE_DIRECTION.up);
  const isFirstRateRender = useRef(true);

  const [rateChangeBy, setRateChangeBy] = useState(0);

  useEffect(() => {
    updateRatesInterval.current = setInterval(() => {
      const toAdd = direction.current === RATE_VALUE_DIRECTION.up ? 1 : -1;
      isFirstRateRender.current = false;
      setRateChangeBy((currentChangeBy) => currentChangeBy + toAdd);
    }, RATE_VALUE_CHANGE_INTERVAL);

    changeDirectionInterval.current = setInterval(() => {
      direction.current =
        direction.current === RATE_VALUE_DIRECTION.down
          ? RATE_VALUE_DIRECTION.up
          : RATE_VALUE_DIRECTION.down;
    }, RATE_VALUE_DIRECTION_INTERVAL);

    stopRateUpdatesTimeout.current = setTimeout(() => {
      clearInterval(updateRatesInterval.current);
      clearInterval(changeDirectionInterval.current);
    }, RATE_VALUE_STOP_CHANGE_TIMEOUT);

    return () => {
      setRateChangeBy(0);

      clearTimeout(stopRateUpdatesTimeout.current);
      clearInterval(updateRatesInterval.current);
      clearInterval(changeDirectionInterval.current);
    };
  }, [rates]);

  return rates.map((rate) => {
    return (
      <Rate
        baseCurrency={baseCurrency}
        currency={rate.currencyName}
        value={rate.value + rateChangeBy * RATE_VALUE_CHANGE_STEP}
        direction={
          isFirstRateRender.current
            ? RATE_VALUE_DIRECTION.stall
            : direction.current
        }
      />
    );
  });
}
