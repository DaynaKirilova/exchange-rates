export const RATE_VALUE_DIRECTION = {
  stall: "stall",
  up: "up",
  down: "down",
};

export const MIN_RATE_VALUE = 1.0001;
export const RATE_VALUE_CHANGE_INTERVAL = 1000 * 5;
export const RATE_VALUE_DIRECTION_INTERVAL = 1000 * 60;
export const RATE_VALUE_STOP_CHANGE_TIMEOUT = 1000 * 60 * 5;

export const RATE_VALUE_CHANGE_STEP = 0.0001;
